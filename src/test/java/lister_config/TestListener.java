package lister_config;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Run before each testcase is start");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Run before each testcase is end");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Take screenshot and attach to report");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Run before each method is skip");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("Run after each testcase if faild xxxx%");
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Run before each class");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("Run after each class");
    }
}
