package tests;

import core.BaseTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Day09_WebDriver_Commnands extends BaseTest {
    @BeforeMethod
    public void startWebsite(){
//        String url = "https://demoqa.com/alerts";
//        driver.get(url);
    }
    @Test(enabled=false)
    public void simpleAlert() {
        String url = "https://demoqa.com/alerts";
        driver.get(url);
        WebElement btnSimpleAlert = driver.findElement(By.id("alertButton"));
        btnSimpleAlert.click();
        driver.switchTo().alert().accept();
    }

    @Test(enabled=false)
    public void confirmAlert() {
        String url = "https://demoqa.com/alerts";
        driver.get(url);
        WebElement btnSimpleAlert = driver.findElement(By.id("confirmButton"));
        btnSimpleAlert.click();
        driver.switchTo().alert().dismiss();
    }

    @Test(enabled=false)
    public void promptAlert() throws InterruptedException {
        String url = "https://demoqa.com/alerts";
        driver.get(url);
        WebElement btnSimpleAlert = driver.findElement(By.id("promtButton"));
        btnSimpleAlert.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.sendKeys("Huyen Le");
        alert.accept();
        Thread.sleep(5000);
    }

    @Test(enabled=false)
    public void authenticationAlert() throws InterruptedException {
      //Điền username và password vào link http://username:password@URL
        String url = "https://the-internet.herokuapp.com/basic_auth";
        String [] lists = url.split("//");
        for (String item : lists) {
            System.out.println(item);
        }
        String username = "admin";
        String password = "admin";
        String newUrl = lists[0]+ "//" + username + ":" + password + "@" + lists[1];
        driver.get(newUrl);
    }

    @Test(enabled=false)
    public void customSelectOneValueDroplist() throws InterruptedException {
        String url = " https://demoqa.com/select-menu";
        driver.get(url);

        WebElement btnSelectOne = driver.findElement(By.id("selectOne"));
        btnSelectOne.click();
        WebElement itemMrs = driver.findElement(By.xpath("//div[text()='Mrs.']"));
        itemMrs.click();
    }

    @Test(enabled=false)
    public void defaultSelectOneValueDroplist() throws InterruptedException {
        String url = " https://demoqa.com/select-menu";
        driver.get(url);
        //Click vào menu
        Select listColors = new Select(driver.findElement(By.id("oldSelectMenu")));
        //get ra tổng số option
        listColors.getOptions();
        System.out.println("option:" + listColors.getOptions().size());
        //Kiểm tra xem dropdowlist có cho phép chọn nhiều hay không
        System.out.println("multiple : " + listColors.isMultiple());
        //Random
        Random random = new Random();
        int index = random.nextInt( listColors.getOptions().size() - 1);
        //3 cách để select value
        listColors.selectByIndex(index);
        listColors.selectByValue("3");
        listColors.selectByVisibleText("Purple");
        Thread.sleep(300);
    }

    @Test(enabled=false)
    public void specialEvents1() throws InterruptedException {
        String url = "https://demo.guru99.com/test/simple_context_menu.html";
        driver.get(url);
        WebElement btnRightClick = driver.findElement(By.xpath("//span[text()='right click me']"));

        Actions action = new Actions(driver);
        action.contextClick(btnRightClick).build().perform();
        WebElement btnEdit = driver.findElement(By.xpath("//span[text()='Edit']"));
        btnEdit.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
    }

    @Test(enabled=false)
    public void jsExcuterHighlight() throws InterruptedException {
        String url = "https://demo.guru99.com/test/simple_context_menu.html";
        driver.get(url);
        WebElement btnRightClick = driver.findElement(By.xpath("//span[text()='right click me']"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='3px dotted blue'",btnRightClick);
        Thread.sleep(3000);
    }

    public String getTimeDateNow() throws InterruptedException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        System.out.println(formatter.format(date));
        return formatter.format(date);
    }

    @Test
    public void statusElement() throws InterruptedException {
        String url = "https://www.facebook.com/";
        driver.get(url);
        WebElement btnCreateNewAcc = driver.findElement(By.xpath("//a[contains(text(),'Create New Account')]"));
//        btnCreateNewAcc.click();
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Create New Account')]")));
        btnCreateNewAcc.click();
        Thread.sleep(3000);

    }
}
