package tests;

import core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

import static utils.extentreports.ExtentTestManager.startTest;

public class Calendar_1 extends BaseTest {

    @BeforeMethod
    public void startWebsite(){
        String url = "https://demo.guru99.com/test/";
        driver.get(url);
    }
    @Test( description = "Invalid Login Scenario with empty username and password.")
    public void sendKeysDateToCalendar(Method method) throws InterruptedException {
//        startTest(method.getName(), "Invalid Login Scenario with invalid username and password.");
        startTest("huyen dang tesst", "Test Calendar");

        WebElement elementName = driver.findElement(By.xpath("//input[@name='bdaytime']"));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].removeAttribute('type')",elementName);
        elementName.sendKeys("09/28/2022");
        WebElement btnSubmit = driver.findElement(By.xpath("//input[@type='submit']"));
        btnSubmit.click();
        Thread.sleep(5000);
    }

    public WebElement elementOfCountry(String country) {
        WebElement btnSubmit = driver.findElement(By.xpath("//input[@type='" + country + "' }`]"));
        return btnSubmit;
    }


}
