package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FacebookHomePage {
    private WebDriver driver;

    @FindBy(id="email")
    public WebElement weEmail;

    @FindBy(id="pass")
    public WebElement wsPassword;

    @FindBy(id="loginbutton")
    public WebElement wsTtnLogin;

    public FacebookHomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void enterFullName(String arg1) {
        weEmail.sendKeys(arg1);
    }
}
