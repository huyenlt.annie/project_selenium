package tests;

import core.BaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.Objs.LoginPageObj;
import provider.DataLogin;

public class LoginPageTest extends BaseTest {
    LoginPageObj objLoginPage;


    @BeforeMethod
    public void setUp() throws Exception {
        objLoginPage = new LoginPageObj(driver);
    }

//    @DataProvider(name = "data_login")
//    public Object[][] dataLogin() throws Exception {
//        String path = System.getProperty("user.dir");
//        Object[][] testObj = ExcelUtils.getTableArray(path + "\\src\\test\\resources\\data_test.xlsx", "Login");
//        return testObj;
//    }

    @Test(dataProvider = "data_login", dataProviderClass = DataLogin.class)
    public void TC_01_Login_Success(String username, String password){
        objLoginPage.setDataToTxtUserName(username);
        objLoginPage.setDataToTxtPassword(password);
    }
}
