package tests;

import lister_config.TestListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class Test_Depend_On {
    @Test
    public void TC01_Create_New_Acc() {

    }

    @Test(dependsOnMethods = "TC01_Create_New_Acc")
    public void TC02_View_Acc() {
        Assert.assertTrue(false);
    }

    @Test(dependsOnMethods = "TC02_View_Acc")
    public void TC03_Edit_Acc() {

    }

    @Test(dependsOnMethods = "TC03_Edit_Acc")
    public void TC04_Move_Acc() {

    }

    @Test(dependsOnMethods = "TC04_Move_Acc")
    public void TC04_Delete_Acc() {

    }

}
