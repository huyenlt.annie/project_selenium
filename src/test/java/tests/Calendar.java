package tests;

import core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Calendar extends BaseTest {

    @BeforeMethod
    public void startWebsite(){
        String url = "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_date";
        driver.get(url);
    }
    @Test(enabled = false)
    public void sendKeysDateToCalendar() throws InterruptedException {
        driver.switchTo().frame("iframeResult");
        WebElement elementName = driver.findElement(By.xpath("//input[@id='birthday']"));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].removeAttribute('type')",elementName);
        elementName.sendKeys("09-28-2022");
        WebElement btnSubmit = driver.findElement(By.xpath("//input[@type='submit']"));
        btnSubmit.click();
        Thread.sleep(5000);
    }

    public WebElement elementOfCountry(String country) {
        WebElement btnSubmit = driver.findElement(By.xpath("//input[@type='" + country + "' }`]"));
        return btnSubmit;
    }


}
