package tests;

import core.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

public class Day08_WebDriver_Commands extends BaseTest {

    @BeforeMethod
    public void startWebsite(){
        String url = "https://demoqa.com/browser-windows";
        driver.get(url);
    }
    @Test
    public void switchToWindowById() {
        WebElement btnNewWindow = driver.findElement(By.xpath("//button[@id='windowButton']"));
        btnNewWindow.click();

        String parentId = driver.getWindowHandle();

        //Get ra Id của tất cả các tab/window đang có
        Set<String> listWindows = driver.getWindowHandles();
        //Dùng vòng lặp để duyệt qua từng window
        for (String id : listWindows) {
            //Nếu id nào khác với parent id
            if (!id.equals(parentId)) {
                //Thì switch vào id đó
                driver.switchTo().window(id);
                System.out.println("window2: " + driver.getTitle());
                //Muốn quay lại thằng parent -> truyền lại id của thằng parent
                driver.switchTo().window(parentId);
                System.out.println("window1: " + driver.getTitle());
            }
        }
    }
    @Test
    public void switchToWindowByTitle(){
        WebElement btnNewWindow = driver.findElement(By.xpath("//button[@id='windowButton']"));
        btnNewWindow.click();
        //Get ra Id của tất cả các tab/window đang có
        Set<String> listWindows = driver.getWindowHandles();
        //Dùng forEach duyệt qua các giá trị của listWindows
        //Switch vào từng window trước
        //get ta title của window đó
        //Kiểm tra với title mong muốn
        //Nếu = stop không kiểm tra nữa
        for(String item : listWindows){
            System.out.println(item);
            driver.switchTo().window(item);
            String title = driver.getTitle();
            if(title == ""){
                break;
            }
        }
    }
}
