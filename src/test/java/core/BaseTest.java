package core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.logs.Log;

public class BaseTest {
    public WebDriver driver;
    public WebDriver getDriver() {
        return driver;
    }
    @Parameters("browser")
    @BeforeClass
    public void setUp(String browser){
        switch (browser){
            case "Chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--no-sandbox");
                chromeOptions.addArguments("--headless");
                chromeOptions.addArguments("disable-gpu");
                driver = new ChromeDriver(chromeOptions);
                break;
            case "Firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            default:
                throw new RuntimeException("Please input correct the browser name!");
        }
    }

    @AfterClass
    public void tearDown() throws InterruptedException {
        Log.info("Tests are ending!");
        driver.quit();
    }
}
