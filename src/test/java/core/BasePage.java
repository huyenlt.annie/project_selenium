package core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    private long TIMEOUT = 30;
    private WebDriverWait wait;

//    public void waitForVisibility(WebDriver driver, String element){
//        wait = new WebDriverWait(driver , TIMEOUT);
//        wait.until(ExpectedConditions.visibilityOf(By.xpath(element)));
//    }

    public WebElement getFindElementById(WebDriver driver, String element) {
        return driver.findElement(By.id(element));
    }

    public WebElement getFindElementByName(WebDriver driver, String element) {
        return driver.findElement(By.name(element));
    }

    public WebElement getFindElementByLinkText(WebDriver driver, String element) {
        return driver.findElement(By.linkText(element));
    }

    public WebElement getFindElementByPartialLinkText(WebDriver driver, String element) {
        return driver.findElement(By.partialLinkText(element));
    }

    public WebElement getFindElementByCss(WebDriver driver, String element) {
        return driver.findElement(By.cssSelector(element));
    }

    public WebElement getFindElementByXpath(WebDriver driver, String element) {
        return driver.findElement(By.xpath(element));
    }

    public void clickToElementByXpath(WebDriver driver, String element){
        getFindElementByXpath(driver, element).click();
    }

    public void clickToElementByCss(WebDriver driver, String element){
        getFindElementByCss(driver, element).click();
    }

    public void clickToElementByLinkText(WebDriver driver, String element){
        getFindElementByLinkText(driver, element).click();
    }

    public void clickToElementByPartialLinkText(WebDriver driver, String element){
        getFindElementByPartialLinkText(driver, element).click();
    }

    public void setDataToElementByXpath(WebDriver driver, String element, String value) {
        getFindElementByXpath(driver, element).clear();
        getFindElementByXpath(driver, element).sendKeys(value);
    }
}
