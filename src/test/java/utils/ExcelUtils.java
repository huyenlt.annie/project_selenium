package utils;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExcelUtils {
    private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static XSSFCell Cell;
    private static XSSFRow Row;

    public static String getCellData(int rownum, int colnum) throws Exception{
        try{
            Cell = ExcelWSheet.getRow(rownum).getCell(colnum);
            String CellData = null;
            switch (Cell.getCellType()){
                case STRING:
                    CellData = Cell.getStringCellValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(Cell))
                    {
                        CellData = String.valueOf(Cell.getDateCellValue());
                    }
                    else
                    {
                        CellData = String.valueOf((long)Cell.getNumericCellValue());
                    }
                    break;
                case BOOLEAN:
                    CellData = Boolean.toString(Cell.getBooleanCellValue());
                    break;
                case BLANK:
                    CellData = "";
                    break;
            }
            return CellData;
        }catch (Exception e){
            return"";
        }
    }

    public static Object[][] getTableArray(String FilePath, String SheetName)  throws Exception {
        String[][] tabArray = null;
        try {
            FileInputStream ExcelFile = new FileInputStream(FilePath);
            // Access the required test data sheet
            ExcelWBook = new XSSFWorkbook(ExcelFile);
            ExcelWSheet = ExcelWBook.getSheet(SheetName);
            int startRow = 1;
            int startCol = 1;
            int ci,cj;
            int totalRows = ExcelWSheet.getLastRowNum();
            System.out.println("totalRows: " + totalRows);
            // you can write a function as well to get Column count
            int totalCols = ExcelWSheet.getRow(1).getLastCellNum() - 1;

            tabArray=new String[totalRows][totalCols];

            for (int i=startRow;i<=totalRows;i++, ci++) {
                ci = i - 1;
                for (int j=startCol;j<=totalCols;j++, cj++){
                    cj = j - 1;
                    System.out.println("tabArray[ci][cj] " + "ci = " + ci + "cj = " + cj);
                    tabArray[ci][cj]=getCellData(i,j);
                    System.out.println(tabArray[ci][cj]);
                }
            }
        }

        catch (FileNotFoundException e)
        {
            System.out.println("Could not read the Excel sheet");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            System.out.println("Could not read the Excel sheet");
            e.printStackTrace();
        }
        return(tabArray);
    }


}
