package provider;

import org.testng.annotations.DataProvider;
import utils.ExcelUtils;

public class DataLogin {
    @DataProvider(name = "data_login")
    public Object[][] dataLogin() throws Exception {
        String path = System.getProperty("user.dir");
        Object[][] testObj = ExcelUtils.getTableArray(path + "\\src\\test\\resources\\data_test.xlsx", "Login");
        return testObj;
    }
}
