package pages.UIs;

public class LoginUI {
    public static final String USER_NAME = "//input[@type='text']";
    public static final String PASS_WORD = "//input[@type='password']";
    public static final String SIGN_IN = "//button[@class='el-button btn-update el-button--primary']";
}
