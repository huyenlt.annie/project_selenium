package pages.Objs;

import core.BasePage;
import pages.UIs.LoginUI;
import org.openqa.selenium.WebDriver;

public class LoginPageObj extends BasePage {
    WebDriver driver;

    public LoginPageObj(WebDriver driver) {
        this.driver = driver;
    }

    public void setDataToTxtUserName(String value) {
        setDataToElementByXpath(driver, LoginUI.USER_NAME, value);
    }

    public void setDataToTxtPassword(String value) {
        setDataToElementByXpath(driver, LoginUI.PASS_WORD, value);
    }



}
